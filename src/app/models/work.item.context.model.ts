
export interface WorkItemContextModel {
    Activity?: ActivityModel;
    Process?: ProcessModel;
    User?: UserModel;
    Workitem?: WorkitemModel;
    SessionInfoXML?: string;
}

export interface ActivityModel {
    Name?: string;
    Sequence?: number;
}

export interface UserModel {
    MemberID?: string;
    Name?: string;
}

export interface  WorkitemModel {
    CreationDateTime?: number;
    DeadlineDateTime?: number;
    ParticipantName?: string;
    ParticipantType?: string;
    Sequence?: string;
    StartDateTime?: number;
    State?: string;
}

export interface ProcessModel {
    CompleteDateTime?: number;
    CreationDateTime?: number;
    Description?: string;
    ID?: number;
    Initiator?: string;
    InitiatorName?: string;
    Name?: string;
    ProcessDefinitionID?: number;
    ProcessState?: string;
}

/*export interface WIHActionClientModel {
    OPTION_CONTINUE?: string;
    OPTION_STOP?: string;
    OPTION_WAIT?: string;
}*/
