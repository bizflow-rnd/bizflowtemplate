import { NgModule } from '@angular/core';
import { SafeUrlPipe } from '@pipes/safe-url.pipe';
import { SafeHtmlPipe } from '@pipes/safe-html.pipe';


@NgModule({
  declarations: [
    SafeUrlPipe,
    SafeHtmlPipe
  ],
  imports: [

  ],
  exports: [
    SafeUrlPipe,
    SafeHtmlPipe
  ]
})
export class PipesModule { }
