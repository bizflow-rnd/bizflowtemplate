import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataService } from '@services/data.service';
import { CommonService } from '@services/common.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
  ],
  providers: [
    DataService,
    CommonService
  ]
})
export class ServicesModule { }
