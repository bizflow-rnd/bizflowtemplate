
import { Component } from '@angular/core';

@Component({
    selector: 'app-block-ui',
    styles: [`
        :host {
          text-align: center; 
        }
        
        .block-overlay-wrapper {
            z-index: 1000;
            border: none;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-color: rgb(0, 0, 0);
            opacity: 0.6;
            cursor: wait;
            position: fixed;
        }
        
        .block-overlay-content {
            z-index: 1011;
            position: fixed;
            padding: 20px;
            margin: 0;
            width: 30%;
            top: 40%;
            left: 35%;
            text-align: center;
            border: none;
            background-color: rgb(0, 0, 0);
            cursor: wait;
            border-radius: 15px;
            opacity: 0.5
        }
    `],
    template: `
      <div style="display:none"></div>
      <div class="block-overlay-wrapper"></div>
      <div class="block-overlay-content">
        <p style="font-size:30px;color:white;">Please wait...</p>
      </div>
  `
})
export class BlockTemplateComponent {}

