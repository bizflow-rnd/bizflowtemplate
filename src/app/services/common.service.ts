
import { EventEmitter, Injectable } from "@angular/core";

/*
* Usage
* start block ui: this.commonService.blockUI.emit("start");
* stop block ui: this.commonService.blockUI.emit("reset");
*/

@Injectable()
export class CommonService {
    public blockUI: EventEmitter<any> = new EventEmitter();
}

