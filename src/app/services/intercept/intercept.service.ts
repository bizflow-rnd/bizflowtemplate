

import { Injectable, NgModule } from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest, HttpResponse
} from '@angular/common/http';

import { Observable  } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class InterceptService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const cloneReq = req.clone();
    return next.handle(cloneReq).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof  HttpResponse) {
        if (event.body.errorCode) {
            console.log('HttpInterceptor Error: ', event.body.errorCode);
        }
      }
    }, (error) => {
      if (error instanceof HttpErrorResponse) {
        console.error(error);
      }
    }));
  }
}

@NgModule({
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptService, multi: true}
  ]
})

export class InterceptModule {}
