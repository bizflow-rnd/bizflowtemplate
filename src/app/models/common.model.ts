export interface RequestLookups {
  LTYPE?: string;
  CATEGORY?: string;
}

export interface ResponseLookups {
  data?: LookupRow[];
}

export interface LookupRow {
  ID?: string;
  PARENT_ID?: string;
  LTYPE?: string;
  NAME?: string;
  LABEL?: string;
  ACTIVE?: string;
  DISP_ORDER?: number;
}
