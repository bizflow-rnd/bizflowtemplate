import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@components/base.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent extends BaseComponent implements OnInit {

  constructor(private injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    /*this.commonService.blockUI.emit("start");
    setTimeout(() => {
      this.commonService.blockUI.emit("stop");
    }, 3000);*/

  }

}
