# BizflowTemplate (version 1.0)
Angular 7+ template with BizFlow WorkItem handler

##Development Tool
VSCode or IntelliJ, but IntelliJ recommended

##Feature 
1. Main Framework : Angular 7+
2. Supports Bootstrap 4.xx and Angular CDK
3. Supports Block UI

##Build 
1. Change project name (default : bizflow) 
2. Configure "proxy.conf.json"  
  * Change url of "project_context", "project_srs_context" and "bizflow"
3. Configure "env.js" and "env.prod.js" under "src/assets/conf"
4. Remove "package-lock.json"
5. Run "npm install"
6. Run "npm start" (or "npm run build-start" with proxy.conf.json)
7. Run "npm run build-prod" for production

