
import { CommonService } from "@services/common.service";
import { Injector, Renderer2 } from '@angular/core';
import { DataService } from '@services/data.service';

export class BaseComponent {

  public commonService: CommonService;
  public renderer: Renderer2;
  public dataService: DataService;

  constructor(private injectorObj: Injector) {
    this.commonService = injectorObj.get(CommonService);
    this.renderer = injectorObj.get(Renderer2);
    this.dataService = injectorObj.get(DataService);
  }

  /** Close BizFlow WorkItem handler */
  closeBizflow() {
    this.commonService.blockUI.emit("start");

    try {
      if (basicWIHActionClient) {
        basicWIHActionClient.complete();
      } else {
        // Page reloading happens when basicWIHActionClient is NOT existed
        basicWIHActionClient = getWIHActionClient();

        if (basicWIHActionClient) {
          // Try to complete One More time
          basicWIHActionClient.complete();
        } else {
          this.commonService.blockUI.emit("reset");
        }
      }
    } catch (e) {
      this.commonService.blockUI.emit("reset");
    }
  }

  /** Close BizFlow WorkItem handler without confirmation */
  exitWIHWithoutConfirm() {
    // this.commonService.blockUI.emit("reset");
    location.href = envVariable.baseUrl + "/bizflow/bizcoves/wih/action.jsp?wihaction=exit&basicWihReadOnly=true";
  }
}
