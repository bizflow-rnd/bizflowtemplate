import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MainComponent } from '@components/main/main.component';
import { BlockTemplateComponent } from '@components/common/block-ui/block-template.component';

import { InterceptModule } from '@services/intercept/intercept.service';
import { ServicesModule } from '@services/services.module';
import { PipesModule } from '@pipes/pipes.module';
import { BlockUIModule } from 'ng-block-ui';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BlockTemplateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DragDropModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    InterceptModule,
    ServicesModule,
    PipesModule
  ],
  providers: [],
  entryComponents: [
    BlockTemplateComponent
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
