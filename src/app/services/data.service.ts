import { Injectable} from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { CodeConfig, UserInfo } from '@configs/code.config';
import { RequestLookups, ResponseLookups } from '@models/common.model';


@Injectable()
export class DataService {
  readonly prefix;
  readonly user;
  readonly httpOptions;

  constructor(private http: HttpClient) {
    this.prefix = CodeConfig.endPointPreFix;
    this.user = {
      LOGIN_ID: UserInfo.LOGIN_ID,
      MEMBER_ID: UserInfo.MEMBER_ID
    };

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  /** http post for promise */
  private post(endPoint: string, params: any): Promise<any> {
    this.httpOptions['user'] = this.user;
    this.httpOptions['data'] = params;
    return this.http.post<any>(this.prefix + endPoint, this.httpOptions).toPromise();
  }

  /** http post for obserable */
  private post$(endPoint: string, params: any): Observable<any> {
    this.httpOptions['user'] = this.user;
    this.httpOptions['data'] = params;
    return this.http.post<any>(this.prefix + endPoint, this.httpOptions);
  }

  getLookup(params: RequestLookups): Promise<ResponseLookups> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    httpOptions['params'] = params;
    const getDataEndpoint = this.prefix + '/data/get/bf.bf-GetLookUp.json';
    return this.http.get<ResponseLookups>(getDataEndpoint, httpOptions).toPromise();
  }


}
