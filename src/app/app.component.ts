import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { BaseComponent } from '@components/base.component';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Subscription } from 'rxjs';
import { UserInfo } from '@configs/code.config';
import { WorkItemContextModel } from '@models/work.item.context.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit, OnDestroy {
  private keyboardEventHandler: () => void;
  private processId: number;

  private workItemContext: WorkItemContextModel;
  private basicWIHActionClient: any;



  public blockUI$: Subscription;
  @BlockUI() blockUI: NgBlockUI;

  constructor(private injector: Injector) {
    super(injector);

    this.blockUI$ = this.commonService.blockUI.subscribe((operation: any) => {
      this.blockUI[operation]();
    });
  }


  ngOnInit() {
    /** Prevent backward except for inputs which have "type" attribute
     * ex) <input value="123"> : backspace key is not working
     *     <input type="text" value="123"> : backspace key is working
     */

    this.keyboardEventHandler = this.renderer.listen('document', 'keydown', event => {
      if (event.keyCode === 8) {
        let stopEvent = false;

        if (event.target) {
          const nodeName = event.target.nodeName.toUpperCase();
          if (nodeName === 'INPUT' || nodeName === 'TEXTAREA') {
            if (nodeName === 'INPUT') {
              if ((event.target).disabled || (event.target).readOnly) {
                stopEvent = true;
              } else {
                const type = ['text', 'password', 'file', 'email', 'number', 'date', 'color', 'datetime', 'datetime-local', 'month', 'range', 'search', 'tel', 'time', 'url', 'week'];
                if (type.indexOf((event.target).getAttribute('type')) < 0) {
                  stopEvent = true;
                }
              }
            } else {
              if ((event.target).disabled || (event.target).readOnly) {
                stopEvent = true;
              }
            }
          } else {
            stopEvent = true;
          }
        } else {
          stopEvent = true;
        }

        if (stopEvent) {
          event.stopPropagation();
          event.preventDefault();
        }
      }
    });

    if (this.initWIHActionClient()) {

    } else {

    }
  }

  initWIHActionClient(callback?: any) {
    if (basicWIHActionClient) {
      this.basicWIHActionClient = basicWIHActionClient;
      this.workItemContext = basicWIHActionClient.getWorkitemContext();
      if (this.workItemContext && this.workItemContext.Process && this.workItemContext.Process.ID) {
        this.processId = this.workItemContext.Process.ID;
        UserInfo.MEMBER_ID = this.workItemContext.User.MemberID;

        if (UserInfo.MEMBER_ID) {
          this.getUserInfo();
        }

        if (callback) {
          callback();
        }
      }
      return true;
    } else {
      return false;
    }

  }

  // TODO: Need to implement to get login id
  getUserInfo() {
    // UserInfo.LOGIN_ID
  }

  ngOnDestroy() {
    if (this.blockUI$) {
      this.blockUI$.unsubscribe();
    }
  }

}


